from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptForm, CreateCategory, AccountCategory
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def receipts(request):
    items = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": items

    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST, user=request.user)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('/receipts/')
    else:
        form = ReceiptForm(user=request.user)
    context = {
        'form': form,
    }
    return render(request, 'receipts/create.html', context)


@login_required
def category_list(request):
    items = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'categories': items
    }
    return render(request, 'receipts/categories.html', context)


@login_required
def account_list(request):
    items = Account.objects.filter(owner=request.user)
    context = {
        'accounts': items
    }
    return render(request, 'receipts/accounts.html', context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = CreateCategory(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect('/receipts/categories/')
    else:
        form = CreateCategory()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create_category.html', context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountCategory(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect('/receipts/accounts/')
    else:
        form = AccountCategory()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create_account.html', context)
