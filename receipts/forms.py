from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account
from django import forms

class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            'vendor',
            'total',
            'tax',
            'date',
            'category',
            'account',
        )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        if user:
            self.fields['category'].queryset = ExpenseCategory.objects.filter(owner=user)
            self.fields['account'].queryset = Account.objects.filter(owner=user)


class CreateCategory(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = (
            'name',
        )

class AccountCategory(ModelForm):
    class Meta:
        model = Account
        fields = (
            'name',
            'number'
        )
